Data Files Description

1. users.csv - user details 

2. channels.csv - channel details 

3. channels_desc.csv - Additional channel specific descriptions

4. extra_users.csv - Users whose details are not available in "users.csv"

5. channel_join.csv - channel_join messages with user, channel and timestamps. 

6. texts_channel.csv - all text message - with user, channel and timestamp

7. text_mentions.csv - who mentioned whom in a text message - from: who sent the message; to: who was cited; channel and timestamp

8. emoticon_responses.csv - emoticon responses - reaction_by: who reacted; source_message_from - to whose message; 
                            source_message_timestamp - ts of the source message (not emoticon); reaction_name - which emoticon; channel
