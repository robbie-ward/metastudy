---
title: "R Notebook"
output: html_notebook
---

```{r}
require(ggplot2)
```

```{r}
df_projects <- read.csv('projects.csv')
```

```{r}
head(df_projects)
```

# How projects share skills

```{r}
df_skills <- do.call(rbind, lapply(1:nrow(df_projects), function(i){
  name <- df_projects$short_title[i]
  skills <- gsub('\\[|\\]|\'','',df_projects$skills[i])
  skills <- trimws(tolower(strsplit(skills, split = ',')[[1]]))
  N <- length(skills)
  data.frame('Name'=rep(name, N), 'Skills'=skills)
}))
```


```{r}
require(igraph)

G <- graph_from_data_frame(df_skills)
```

```{r}
V(G)$type <- bipartite_mapping(G)$type
```


```{r}
Gproj <- bipartite_projection(G)
G_projects <- Gproj$proj1
G_skills <- Gproj$proj2

write.graph(G, 'network_bipartite_projects_skills.graphml', format='graphml')
write.graph(G_skills,'network_skills.graphml',format='graphml')
write.graph(G_projects,'network_projects.graphml',format='graphml')
```




# Who are in the same team



```{r}
df_teams <- do.call(rbind, lapply(1:nrow(df_projects), function(i){
  name <- df_projects$short_title[i]
  members <- df_projects$users_sm[i]
  members <- gsub('\\[|\\]|\'','',members)
  members <- as.character(strsplit(members, split = '\\{')[[1]])[-1]
  
  members <- sapply(1:length(members), function(i){
    paste(sapply(strsplit(members[i],',')[[1]][2:3], function(x) trimws(strsplit(x,':')[[1]][2])),collapse=' ')
  })
  
  N <- length(members)
  data.frame('Name'=rep(name, N), 'Users'=members)
}))
```


```{r}
G <- graph_from_data_frame(df_teams)
```

```{r}
V(G)$type <- bipartite_mapping(G)$type
G
```

```{r}
Gproj <- bipartite_projection(G)
G_projects <- Gproj$proj1
G_users <- Gproj$proj2

write.graph(G, 'network_bipartite_user_project.graphml', format='graphml')
write.graph(G_users,'network_users.graphml',format='graphml')
write.graph(G_projects,'network_projects_users.graphml',format='graphml')
```
