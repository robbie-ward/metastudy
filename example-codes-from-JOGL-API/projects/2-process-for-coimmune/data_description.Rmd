---
title: "R Notebook"
output: html_notebook
---

```{r}
require(ggplot2)
```

```{r}
df_projects <- read.delim('../1-extract-data/projects_jogl.tsv')
```

```{r}
head(df_projects)
```


```{r}
df_users <- read.delim('../../users/1-extract-data/users_jogl.tsv')
```


```{r, fig.width=3, fig.asp=1}
challenges <- unlist(sapply(df_projects$Challenges_num, function(x) strsplit(as.character(x),',')[[1]]))
tb <- table(challenges)
df <- data.frame('Category'=paste('Challenge',names(tb)),
                'Number'=as.numeric(tb))



ggplot(data=df, aes(x=Category, y=Number)) +
  geom_bar(stat="identity", fill="steelblue") +
    geom_text(aes(label=Number), vjust=-0.3, size=3.5)+
  xlab('')+
  ylab('Number of projects')+
  theme(text = element_text(size=16),
    axis.text.x = element_text(angle = 90, hjust = 1))
```


```{r}
ggsave('plotChallengesNumberProjects.png')
```

```{r}
t <- read.delim('list-of-projects.tsv')
head(t)

```
```{r}
inds_coimmune <- which(df_projects$id %in% t$ID)
```



# Who are in the same team


```{r}
df_teams <- do.call(rbind, lapply(inds_coimmune, function(i){
  name <- df_projects$short_title[i]
  users <- strsplit(as.character(df_projects$Users_num[i]), split = ',')[[1]]
  N <- length(users)
  inds <- match(users,df_users$id)
  user_names <- paste(df_users$first_name[inds], df_users$last_name[inds])
  data.frame('Name'=rep(name, N), 'Users'=user_names, 'ids'=df_users$id[inds])
}))
```

```{r}
user_ids = df_teams$ids
```



```{r}
require(igraph)

G <- graph_from_data_frame(df_teams)
```

```{r}
V(G)$type <- bipartite_mapping(G)$type
G
```

```{r}
Gproj <- bipartite_projection(G)
G_projects <- Gproj$proj1
G_users <- Gproj$proj2

write.graph(G, 'network_bipartite_user_project.graphml', format='graphml')
write.graph(G_users,'network_user_teams.graphml',format='graphml')
write.graph(G_projects,'network_projects_users.graphml',format='graphml')
```

```{r, fig.width=3, fig.asp=1}
tb <- degree(G)[which(V(G)$type==0)]
names(tb)[names(tb)==''] <- 'Unknown'
df <- data.frame('Category'=names(tb),
                'Number'=as.numeric(tb))

df$Category <- factor(df$Category,levels = df$Category[order(-df$Number)])


ggplot(data=df, aes(x=Category, y=Number)) +
  geom_bar(stat="identity", fill="steelblue") +
    geom_text(aes(label=Number), vjust=-0.3, size=3.5)+
  xlab('')+
  ylab('Number of members')+
  theme(text = element_text(size=16),
    axis.text.x = element_text(angle = 90, hjust = 1))

ggsave('plotNumMembers.png')
```


# How projects share skills


```{r}

df_skills <- do.call(rbind, lapply(inds_coimmune, function(i){
  name <- df_projects$short_title[i]
  skills <- gsub('\\[|\\]|\'','',df_projects$skills[i])
  skills <- trimws(tolower(strsplit(skills, split = ',')[[1]]))
  N <- length(skills)
  data.frame('Name'=rep(name, N), 'Skills'=skills)
}))
```


```{r}
require(igraph)

G <- graph_from_data_frame(df_skills)
```

```{r}
V(G)$type <- bipartite_mapping(G)$type
write.graph(G,'network_bipartite_projects_skills.graphml',format='graphml')
```

```{r}
Gproj <- bipartite_projection(G)
G_proj_skills <- Gproj$proj1

write.graph(G_proj_skills,'network_projects_skills.graphml',format='graphml')
```





```{r}
G1 <- read.graph('network_projects_users.graphml',format='graphml')
G2 <- read.graph('network_projects_skills.graphml',format='graphml')
write.graph(G1+G2, file='network_projects_users_skills.graphml',format='graphml')
```

```{r}
Gf <-G1+G2

w1 <- E(Gf)$weight_1
w1[is.na(w1)] <- 0
w2 <- E(Gf)$weight_2
w2[is.na(w2)] <- 0


```

```{r}
plot.0(w1,w2)
```

