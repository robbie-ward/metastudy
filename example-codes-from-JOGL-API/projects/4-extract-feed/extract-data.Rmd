---
title: "R Notebook"
output: html_notebook
---

```{r}
t <- read.delim('feeds.tsv')
```

```{r}
ids <- na.omit(t$feed_id)
```

```{r}
dir.create('feeds', showWarnings = F)
for (id in ids){
  system(paste0('curl --location --request GET https://jogl-backend.herokuapp.com/api/feeds/',id,
                ' > feeds/',id,'.json'))
}
```


